package com.tulin.v8.ide.views.navigator.filters;

import org.eclipse.jdt.internal.ui.viewsupport.MemberFilter;

@SuppressWarnings("restriction")
public class FieldsFilter extends MemberFilter {
	public FieldsFilter() {
		addFilter(4);
	}
}
